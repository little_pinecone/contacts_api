package com.martaszymek.contacts.controller;

import com.martaszymek.contacts.model.*;
import com.martaszymek.contacts.service.PersonService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.Arrays;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest(PersonController.class)
public class PersonControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PersonService personService;

    @Test
    public void getAllPeople() throws Exception {
        Person person = new Person();
        person.setFirstName("Marian");
        person.setLastName("Koniuszko");
        person.setEmailAddresses(Arrays.asList(new EmailAddress("test@test.com")));
        person.setPhoneNumbers(Arrays.asList(new PhoneNumber("11111111")));
        Address address = new Address();
        address.setCity("Lublin");
        address.setPostalCode("20-200");
        address.setHomeDetails("2a");
        person.setAddresses(Arrays.asList(address));
        person.setIdentifierNumber("1");
        person.setDateOfBirth(LocalDate.of(1768,05,16));
        person.setSex(Sex.MALE);
        when(personService.getAllPeople()).thenReturn(Arrays.asList(person));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/api/people/").accept(
                MediaType.APPLICATION_JSON);

        MvcResult result = mvc.perform(requestBuilder).andReturn();

        String expected = "[{\n" +
                "    \"firstName\": \"Marian\",\n" +
                "    \"lastName\": \"Koniuszko\",\n" +
                "    \"identifierNumber\": \"1\",\n" +
                "    \"phoneNumbers\": [{\"value\": \"11111111\"}],\n" +
                "    \"emailAddresses\": [{\"value\": \"test@test.com\"}],\n" +
                "    \"addresses\": [{ \"homeDetails\":\"2a\", \"postalCode\":\"20-200\", \"city\":\"Lublin\"}],\n" +
                "    \"dateOfBirth\": \"1768-05-16\",\n" +
                "    \"sex\": \"MALE\"\n" +
                "}]";


        JSONAssert.assertEquals(expected, result.getResponse()
                .getContentAsString(), false);
    }
}