package com.martaszymek.contacts.model;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;

@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Please provide postal code")
    @Pattern(regexp = "\\d{2}-\\d{3}", message = "Please provide a valid postal code")
    private String postalCode;

    @NotBlank(message = "Please provide a city")
    private String city;

    private String streetName;

    @NotBlank(message = "Please provide a home number")
    private String homeDetails;

    private String apartmentDetails;

    public Long getId() {
        return id;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getHomeDetails() {
        return homeDetails;
    }

    public void setHomeDetails(String homeDetails) {
        this.homeDetails = homeDetails;
    }

    public String getApartmentDetails() {
        return apartmentDetails;
    }

    public void setApartmentDetails(String apartmentDetails) {
        this.apartmentDetails = apartmentDetails;
    }

    @Override
    public String toString() {
        String[] address = {streetName, homeDetails, apartmentDetails, postalCode, city};
        return String.join(" ", address);
    }
}
