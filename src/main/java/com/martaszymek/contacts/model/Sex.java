package com.martaszymek.contacts.model;

public enum  Sex {
    MALE("M"), FEMALE("F");
    private String value;

    Sex(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
