package com.martaszymek.contacts.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.Valid;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@JsonIgnoreProperties(allowGetters = true)
public class Person implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Please provide a first name")
    private String firstName;

    @NotBlank(message = "Please provide a last name")
    private String lastName;

    @NotBlank(message = "Please provide an identifier number")
    @Column(unique = true)
    private String identifierNumber;

    private LocalDate dateOfBirth;

    @Enumerated(EnumType.STRING)
    private Sex sex;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "person_id", nullable = false)
    @Valid
    private List<PhoneNumber> phoneNumbers;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "person_id", nullable = false)
    @Valid
    private List<EmailAddress> emailAddresses;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "person_id", nullable = false)
    @Valid
    private List<Address> addresses;

    public Person() {
        phoneNumbers = new ArrayList<>();
        emailAddresses = new ArrayList<>();
        addresses = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getIdentifierNumber() {
        return identifierNumber;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setIdentifierNumber(String identifierNumber) {
        this.identifierNumber = identifierNumber;
    }

    public List<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
        this.phoneNumbers.clear();
        this.phoneNumbers.addAll(phoneNumbers);
    }

    public List<EmailAddress> getEmailAddresses() {
        return emailAddresses;
    }

    public void setEmailAddresses(List<EmailAddress> emailAddresses) {
        this.emailAddresses.removeIf(oldMail -> notInList(emailAddresses, oldMail));
        List<EmailAddress> newEmails = getNewEmailAddresses(emailAddresses);
        this.emailAddresses.addAll(newEmails);
    }

    private List<EmailAddress> getNewEmailAddresses(List<EmailAddress> emailAddresses) {
        return emailAddresses.stream()
                    .filter(newMail -> notInList(this.emailAddresses, newMail))
                    .collect(Collectors.toList());
    }

    private boolean notInList(List<EmailAddress> emailAddresses, EmailAddress thisMail) {
        return !emailAddresses.contains(thisMail);
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses.clear();
        this.addresses.addAll(addresses);
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }
}
