package com.martaszymek.contacts.controller;

import com.martaszymek.contacts.model.Person;
import com.martaszymek.contacts.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PersonController {

    private PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/people")
    public List<Person> getAllPeople() {
        return personService.getAllPeople();
    }

    @PostMapping("/people")
    public Person savePerson(@Valid @RequestBody Person person) {
        return personService.savePerson(person);
    }

    @GetMapping("/people/{id}")
    public ResponseEntity<Person> getPersonById(@PathVariable(value = "id")Long personId) {
        return personService.getPersonById(personId);
    }

    @PutMapping("/people/{id}")
    public ResponseEntity<Person> updatePerson(
            @PathVariable(value = "id") Long personId,
            @Valid @RequestBody Person personDetails
    ) {
        return personService.updatePerson(personId, personDetails);
    }

    @DeleteMapping("/people/{id}")
    public ResponseEntity<Person> deletePerson(@PathVariable(value = "id") Long personId) {
        return personService.deletePerson(personId);
    }
}
