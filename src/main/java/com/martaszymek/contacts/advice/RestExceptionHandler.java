package com.martaszymek.contacts.advice;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.martaszymek.contacts.error.ApiError;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.stream.Collectors;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler{

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Throwable mostSpecificCause = ex.getMostSpecificCause();
        if(mostSpecificCause instanceof InvalidFormatException) {
            InvalidFormatException invalidFormatException = (InvalidFormatException) mostSpecificCause;

            StringBuilder sb = buildErrorMessage(invalidFormatException);
            return new ResponseEntity(new ApiError(status, sb.toString(), sb.toString()), headers, status);
        }
        return new ResponseEntity(new ApiError(status, ex.getMessage(), ex.getMessage()), headers, status);
    }

    private StringBuilder buildErrorMessage(InvalidFormatException invalidFormatException) {
        StringBuilder sb = new StringBuilder();
        sb.append("Invalid value for ");
        sb.append(invalidFormatException.getPath()
                .stream()
                .map(p -> p.getFieldName().toString())
                .collect(Collectors.joining(", ")));
        sb.append(": ");
        sb.append(invalidFormatException.getValue().toString());
        return sb;
    }

}
