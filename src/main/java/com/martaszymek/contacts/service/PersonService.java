package com.martaszymek.contacts.service;

import com.martaszymek.contacts.model.Person;
import com.martaszymek.contacts.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {

    private PersonRepository personRepository;

    @Autowired
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getAllPeople() {
        return personRepository.findAll();
    }

    public Person savePerson(Person person) {
        return personRepository.save(person);
    }

    public ResponseEntity<Person> getPersonById(Long personId) {
        Person person = personRepository.findOne(personId);
        if(person == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(person);
    }

    public ResponseEntity<Person> updatePerson(Long personId,Person personDetails) {
        Person person = personRepository.findOne(personId);
        if(person == null) {
            return ResponseEntity.notFound().build();
        }
        Person updatedPerson = savePerson(personDetails, person);
        return ResponseEntity.ok(updatedPerson);
    }

    private Person savePerson(Person personDetails, Person person) {
        person.setFirstName(personDetails.getFirstName());
        person.setLastName(personDetails.getLastName());
        person.setIdentifierNumber(personDetails.getIdentifierNumber());
        person.setPhoneNumbers(personDetails.getPhoneNumbers());
        person.setEmailAddresses(personDetails.getEmailAddresses());
        person.setAddresses(personDetails.getAddresses());
        person.setDateOfBirth(personDetails.getDateOfBirth());
        person.setSex(personDetails.getSex());
        return personRepository.save(person);
    }

    public ResponseEntity<Person> deletePerson(Long personId) {
        Person person = personRepository.findOne(personId);
        if(person == null) {
            return ResponseEntity.notFound().build();
        }
        personRepository.delete(person);
        return ResponseEntity.ok().build();
    }
}
